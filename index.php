<?php
if (is_file("configs.php")) {
  include_once 'configs.php';
} else {
  header("location:install.php");
  }
try {
  include_once 'functions.php';
  $pdo = connect();
  $limit = 20;
  $query = "SELECT idn,title,content,date FROM spost";
  $s = $pdo->prepare($query);
  $s->execute();
  $total_results = $s->rowCount();
  $total_pages = ceil($total_results/$limit);
  if (!isset($_GET['page'])) {
    $page = 1;
  } else {
    $page = $_GET['page'];
    }
  if (!isset($_GET['date'])) {
    $date = ASC;
  } else {
    $date = $_GET['date'];
    }       
  $starting_limit = ($page-1)*$limit;
  $a = ASC;
  if ($date == $a) {
    $sql = "SELECT idn,title,content,date FROM spost ORDER BY Date ASC lIMIT $starting_limit , $limit";
  } else {
    $sql = "SELECT idn,title,content,date FROM spost ORDER BY Date DESC lIMIT $starting_limit , $limit";
    }             
  $q = $pdo->prepare($sql);
  $q->execute([$date]);               
  $q = $pdo->query($sql);
  $q->setFetchMode(PDO::FETCH_ASSOC);
  } catch (PDOException $e) {
    die("Could not connect to the database $dbname :" . $e->getMessage());
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>



  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">


  <style>
    .disabled {
      pointer-events:none;
      opacity:0.0;        
    }
    .ralign {
      position: absolute;
      right: 0px;
    }
    </style>

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Start Bootstrap</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add-blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="cat.php">Category</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Clean Blog</h1>
            <span class="subheading">A Blog Theme by Start Bootstrap</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
      <div class="post-preview">
      <div class="dropdown">
    <button class="btn btn-primary dropdown-toggle" style="float: right;" type="button" data-toggle="dropdown">Sort by:
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li><a href="#"></a></li>
      <li><a href = "index.php?date=ASC&page=<?php if (isset($_GET['page'])) { echo  $_GET['page'];} else {echo 1;}?>">DATE-Ascending</a></li>
      <li><a href="index.php?date=DESC&page=<?php if (isset($_GET['page'])) { echo  $_GET['page'];} else {echo 1;}?>">DATE-Descendingt</a></li>
    </ul>
  </div>

          <a href = "sql.php">
            <h2 class = "post-title">
           <?php
              while ($row = $q->fetch()): {
                $val = $row["idn"];
                $str = $row["content"];
                // $a = explode(" ",$str);
                $cont = trimm($str);

                echo '
                  <a href = "sql.php?id='.$val.'">
                  <h2 class = "post-title">'.$row["title"].'</h2>
                  <h4 class =  "post-subtitle text-muted">'.$cont.'</h4>
                  <h6 class = "text-muted post-meta">Posted by
                  <a href = "#">Start Bootstrap</a>
                  on '.$row["date"].'</h6>
                  </a>
                  ';  
                $stmt2 = $pdo->prepare("SELECT tags.tag,tags.tid FROM relation,tags WHERE relation.blog_id = ? AND tags.tid=relation.tag_id");
                $stmt2->execute([$val]);
                echo "<p>Tags:";
                while ($row2 = $stmt2->fetch()) {
                  echo '<a href = "tag.php?tag='.$row2['tid'].'">'.$row2['tag'].",".'</a></p>';
                }
                  $stmt3 = $pdo->prepare("SELECT category.categories,category.cid FROM relation,category WHERE relation.blog_id = ? AND category.cid=relation.cat_id");
                  $stmt3->execute([$val]);
                  echo "Category:";
                  while ($row3 = $stmt3->fetch()) {
                    echo '<a href = "tag.php?tag='.$row3['cid'].'">'.$row3['categories'].",".'</a>';
                  }
                
                echo'<hr>'; 
              }  
              endwhile;
            ?>


<div class = "clearfix">
<ul class="pagination">  
            <li  class="<?php if($page == 1){ echo 'disabled'; } ?>">
              <a href="<?php if($page == 1){ echo '#'; } else { echo "index.php?sort=$date&page=".($page - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if($page == $total_pages){ echo 'disabled'; } ?>">
              <a class="ralign" href="<?php if($page == $total_pages){ echo '#'; } else { echo "index.php?sort=$date&page=".($page + 1); } ?>">  Next</a>
            </li>  
          </ul>         
        </div>
      </div>
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>


</body>

</html>