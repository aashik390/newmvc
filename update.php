<script>
var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
</script>

<?php
require 'functions.php';
$conn = connect();
$data = "SELECT  cid,categories from category";
$res = $conn->prepare($data);
$res->execute();
$data = $res->fetchall();
                   
$message = "";
$id = $_GET['id'];
if (($id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT)) !== false && $id > 0) {
  $stmt = $conn->prepare("SELECT  title,content FROM  spost WHERE idn=?");
  $stmt->execute([$id]);
  $row = $stmt->fetch();
  $stmt2 = $conn->prepare("SELECT tags.tag,tags.tid FROM relation,tags WHERE relation.blog_id = ? AND tags.tid = relation.tag_id");
  $stmt2->execute([$id]);
}       
if (isset($_POST['edit'])) {
  if (isset($_POST['title'])) {
    $title = $_POST['title'];
  }
  if (isset($_POST['blog'])) {
    $blog = $_POST['blog'];
  }
  if (isset($_POST['tags'])) {
    $tag = $_POST['tags'];
  }
  $tag = trim($tag," ");
  $tag = strtolower($tag);
  $wordsarray = explode(",",$tag);
  $s = sizeof($wordsarray);
  for ($i = 0; $i < $s; $i++) {
    $wordsarray[$i]=trim($wordsarray[$i]);
  }
  try {  
    $blog = addslashes($blog);
    $sql = "UPDATE spost SET title = '$title',content = '$blog' WHERE idn = '$id'" ;
    $conn->exec($sql);
    $reldel = "DELETE from relation where blog_id = '$id'";
    $conn->exec($reldel); 
    for($i = 0; $i < $s; $i++) {
      $tagurl = "SELECT  tag FROM tags  WHERE  tag = '$wordsarray[$i]'";
      $result = $conn->query($tagurl);
      $result->setFetchMode(PDO::FETCH_ASSOC);
      $count = $result->rowCount();
      if ($count == 0) {
        $tagsq = "INSERT INTO tags (tag)  VALUES('$wordsarray[$i]')";
        $conn->exec($tagsq);
      }
      foreach($_POST['list'] as $s) {     
        $sql = "SELECT cid FROM category WHERE categories = '$s'";
        $q = $conn->query($sql);
        $q->setFetchMode(PDO::FETCH_ASSOC);
        $cid = $q->fetch();
        $sql= "INSERT INTO relation (blog_id, tag_id, cat_id)
                SELECT bd.idn, tt.tid, cc.cid FROM spost bd
                JOIN  tags tt ON bd.title = '$title' AND tt.tag = '$words[$i]'
                JOIN category cc ON bd.title = '$title' AND cc.categories = '$s'";
                $conn->exec($sql);
      }
      $delete = "DELETE FROM tags WHERE tid NOT IN (SELECT tag_id FROM relation)";
      $conn->query($delete);
      echo "<script type= 'text/javascript'>alert('Updated successfull');</script>";
    } 
    Header("Location: sql.php?id=".$id);
  } catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
    }
}
?>
  
<!DOCTYPE html>
<html lang="en">
 
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
 
  <title>Clean Blog - Start Bootstrap Theme</title>
 
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/clean-blog.css" rel="stylesheet">
 
  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
 
  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
 
</head>
 
<body>
 
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand" href="index.php">Start Bootstrap</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
            </li>
        </ul>
      </div>
    </div>
  </nav>
 
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/blog-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="page-heading">
            <h1>Edit your blog contents here</h1>
            <span class="subheading">Lets start blogging...</span>
          </div>
        </div>
      </div>
    </div>
  </header>
 
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <form name="blogform"  method="POST">
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Title</label>
              Edit title:
              <input type="text" class="form-control"  value="<?php echo $row['title']; ?>" name="title" required data-validation-required-message="Please enter your name.">
              <p class="help-block text-danger"></p>
            </div>
          </div>
         
          <div class="control-group">
            <div class="form-group floating-label-form-group controls">
              <label>Blog</label>
              Edit Content:            
              <textarea rows="5" class="form-control" placeholder="Edit blog"  name="blog" required data-validation-required-message="Please type your content."><?php echo $row['content']; ?></textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="control-group">
          Edit tag:
            <div class="form-group floating-label-form-group controls">
              <textarea  class="form-control"  name="tags" required >
              <?php
                while ($row2 = $stmt2->fetch()) {
                  echo $row2['tag'].",";
                }
              ?>
              </textarea>
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <br>

          <div class="multiselect">
            <div class="selectBox" onclick="showCheckboxes()">
              <select>
                <option>Select category(s)</option>
              </select>
              <div class="overSelect"></div>
            </div>
            <div id="checkboxes">
            <?php
              if (isset($data)) {
                foreach ($data as $row) {
                  $id = $row['cid'];
                  echo '<label for="one"> 
                  <input type="checkbox" name="list[]" id="'.$id.'" value="'.$row['categories'].'" />'.$row['categories'].'</label>';
                }
              }
            ?>
            </div>
          </div>
           

          <div id="success"></div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary" name= "edit" id="AddBlogButton">UPDATE</button>
          </div>
        </form>
     
      </div>
    </div>
  </div>
 
  <hr>
 
  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>
 
  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
 
 
  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>
 
</body>
 
</html>