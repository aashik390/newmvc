<?php
if (isset($_GET['page'])) {
  $pageno = $_GET['page'];
} else {
  $pageno = 1;
}
include_once 'functions.php';
$conn = connect();
$tagidvalue=$_GET['tag'];
$n = 2;
$total_pages_sql = "SELECT idn,title,content,date,tag_id,blog_id FROM spost,relation WHERE spost.idn=relation.blog_id  AND relation.tag_id=$tagidvalue";
$q1 = $conn->query($total_pages_sql);
$total_rows = $q1->rowCount();
$total_pages = ceil($total_rows / $n);
$sort=$_GET['date'];
$a=ASC;
if ($pageno>$total_pages){
  $pageno =1;
}
$offset = ($pageno-1) * $n;
if($sort == $a) {
  $q="SELECT idn,title,content,date,tag_id,blog_id FROM spost,relation 
  WHERE spost.idn=relation.blog_id  AND relation.tag_id=? ORDER BY Date ASC lIMIT $offset,$n";
}
else {
  $q="SELECT idn,title,content,date,tag_id,blog_id FROM spost,relation 
  WHERE spost.idn=relation.blog_id  AND relation.tag_id=? ORDER BY Date DESC lIMIT $offset,$n";
}
$s = $conn->prepare($q);
$s->execute([$tagidvalue]);
$d = $s->fetchAll();    
?> 
<!DOCTYPE html>
<html lang="en">
 
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
 
  <title>Test Blog</title>
 
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" re<!DOCTYPE html>
<html lang="en">

<style>

  .disabled {
    pointer-events:none;
    opacity:0.0;        
  }

 .ralign {
    position: absolute;
    right: 0px;
  }

</style>
 
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
 
  <title>Test Blog</title>
 
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
 
  <!-- Custom fonts for this template -->
  <link href="css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
 
  <!-- Custom styles for this template -->
  <link href="css/clean-blog.min.css" rel="stylesheet">
 
</head>
 
<body>
 
  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
    <a class="navbar-brand" href="index.php">Start Bootstrap</a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
          </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="add.php">Add Blog</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
 
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Tag Posts</h1>
          </div>
        </div>
      </div>
    </div>
  </header>
 
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
                   
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort by:
          <span class="caret"></span></button>
          <ul class="dropdown-menu">  
            <li><a href="tag.php?date=ASC&page=<?php echo $pageno; ?>&tag=<?php echo $_GET['tag']; ?>">DATE-Ascending</a></li>
            <li><a href="tag.php?date=DESC&page=<?php echo $pageno; ?>&tag=<?php echo $_GET['tag']; ?>">DATE-Descending</a></li>
          </ul>
        </div>
                    <?php
                    if (isset($d)) {
                      foreach ($d as $row) {
                        $val = $row["idn"];
                        $str = $row["content"];
                        $words = explode(" ",$str);
                        $cont =  implode(" ", array_splice($words, 0, 200));
                        $sql1 = "SELECT tags.tag,tags.tid FROM relation,tags WHERE relation.blog_id = ? AND tags.tid=relation.tag_id";
                        $st2 = $conn->prepare($sql1);
                        $st2->execute([$val]);
                        $data2 = $st2-> fetchAll();
                        if(str_word_count($cont)>199) {
                          $cont = $cont."...";
                        }
                        echo '
                          <a href ="sql.php?id='.$row['idn'].'">
                          <div class="card" style="width:99%">
                          <div class="card-body">
                          <h2 class="post-title">'.$row["title"].' </h2></a>
                          <h6 class="card-subtitle mb-2 text-muted">'.$row["date"].'</h6>
                          <p h3 class="post-subtitle">'.$cont.'
                          </p>
                        ';
                        echo "<p>Tags: ";
                        if (isset($data2)) {
                          foreach ($data2 as $row2) {
                            $tagidval = $row2["tid"];
                            echo '<a href="tag.php?date='.$sort.'&page='.$pageno.'&tag='.$tagidval.'">'.$row2["tag"].' </a>';      
                          }
                        }
                        echo "</p>
                        </div>
                        <hr>";
                      }  
                    } else {
                      echo "0 results";
                    }
                ?>
 
<div class="clearfix">
           <ul class="pagination">  
            <li  class="<?php if($pageno == 1){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right" href="<?php if($pageno == 1){ echo '#'; } else { echo "tag.php?date=$sort&tag=$tagidvalue&page=".($pageno - 1); } ?>">Prev  </a>
            </li>
            <li  class="<?php if($pageno == $total_pages){ echo 'disabled'; } ?>">
              <a class="btn btn-primary float-right ralign" href="<?php if($pageno == $total_pages){ echo '#'; } else { echo "tag.php?date=$sort&tag=$tagidvalue&page=".($pageno + 1); } ?>">  Next</a>
            </li>  
          </ul>
        </div>  
  <!-- Bootstrap core JavaScript -->
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.bundle.min.js"></script>
 
  <!-- Custom scripts for this template -->
  <script src="js/clean-blog.min.js"></script>
 
</body>
 
</html>